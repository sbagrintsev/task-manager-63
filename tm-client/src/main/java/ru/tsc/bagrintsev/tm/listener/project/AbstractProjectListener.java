package ru.tsc.bagrintsev.tm.listener.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.listener.AbstractListener;
import ru.tsc.bagrintsev.tm.util.DateUtil;

@NoArgsConstructor
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    public IProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String shortName() {
        return "";
    }

    protected void showProject(final ProjectDto project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getDateCreated()));
        System.out.println("STARTED: " + DateUtil.toString(project.getDateStarted()));
        System.out.println("FINISHED: " + DateUtil.toString(project.getDateFinished()));
    }

}
