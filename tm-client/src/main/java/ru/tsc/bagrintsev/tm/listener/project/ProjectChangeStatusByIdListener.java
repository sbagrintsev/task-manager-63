package ru.tsc.bagrintsev.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String description() {
        return "Change project status by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectChangeStatusByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        showParameterInfo(EntityField.STATUS);
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatusValue(statusValue);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status";
    }

}
