package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.tsc.bagrintsev.tm.dto.response.task.TaskListByProjectIdResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskListByProjectIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "List tasks by project id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskListByProjectIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.PROJECT_ID);
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(request);
        @Nullable final List<TaskDto> tasks = response.getTasks();
        int index = 0;
        if (tasks != null) {
            for (final TaskDto task : tasks) {
                System.out.println(++index + ". " + task);
            }
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-list-by-project";
    }

}
