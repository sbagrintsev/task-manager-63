package ru.tsc.bagrintsev.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectClearRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectClearListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        projectEndpoint.clearProject(new ProjectClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

}
