package ru.tsc.sbagrintsev.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectClearRequest;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectCreateRequest;
import ru.tsc.bagrintsev.tm.dto.request.task.*;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.response.project.ProjectCreateResponse;
import ru.tsc.bagrintsev.tm.dto.response.task.*;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.SoapCategory;

import java.security.GeneralSecurityException;
import java.util.List;

@Category(SoapCategory.class)
public class TaskGraphDTOEndpointTest {

    @NotNull
    private final String WRONG = "wrong";

    @NotNull
    private final String TEST = "test";

    @NotNull
    private final String projectName = "junitProject";

    @NotNull
    private final String projectDescription = "junitDescription";

    @NotNull
    private final String taskName = "junitTask";

    @NotNull
    private final String taskDescription = "junitTaskDescription";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @Nullable
    private String tokenTest;

    @NotNull
    private String projectId;

    @Before
    public void setUp() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse responseTest = authEndpoint.signIn(new UserSignInRequest(TEST, TEST));
        tokenTest = responseTest.getToken();
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(tokenTest, projectName, projectDescription)
        );
        projectId = response.getProject().getId();
    }

    @After
    public void tearDown() {
        projectEndpoint.clearProject(new ProjectClearRequest(tokenTest));
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable String prjId = task.getProjectId();
        Assert.assertNull(prjId);
        @NotNull final TaskBindToProjectResponse response1 = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(tokenTest, projectId, taskId)
        );
        Assert.assertNotNull(response1);
        task = response1.getTask();
        Assert.assertNotNull(task);
        prjId = task.getProjectId();
        Assert.assertEquals(projectId, prjId);
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable final String id = task.getId();
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        task = taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(tokenTest, id, "IN_PROGRESS")).getTask();
        Assert.assertNotNull(task);
        Assert.assertNotEquals(Status.NOT_STARTED, task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void testClearTask() {
        taskEndpoint.createTask(new TaskCreateRequest(tokenTest, taskName, taskDescription));
        @Nullable List<TaskDto> tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        taskEndpoint.clearTask(new TaskClearRequest(tokenTest));
        tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testCreateTask() {
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest("", "", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(tokenTest, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(WRONG, null, null))
        );
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable final TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    public void testListTask() {
        taskEndpoint.createTask(new TaskCreateRequest(tokenTest, taskName, taskDescription));
        @Nullable List<TaskDto> tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        taskEndpoint.createTask(new TaskCreateRequest(tokenTest, taskName, taskDescription));
        tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void testListTaskByProjectId() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(tokenTest, projectId, taskId)
        );
        @NotNull final TaskListByProjectIdResponse response1 = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(tokenTest, projectId)
        );
        Assert.assertNotNull(response1);
        List<TaskDto> tasks = response1.getTasks();
        Assert.assertEquals(taskId, tasks.get(0).getId());
    }

    @Test
    public void testRemoveTaskById() {
        taskEndpoint.createTask(new TaskCreateRequest(tokenTest, taskName, taskDescription));
        taskEndpoint.createTask(new TaskCreateRequest(tokenTest, taskName, taskDescription));
        @Nullable List<TaskDto> tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        @NotNull final String idFirst = tasks.get(0).getId();
        @NotNull final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(tokenTest, idFirst));
        Assert.assertNotNull(response);
        tasks = taskEndpoint.listTask(new TaskListRequest(tokenTest, null)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void testShowTaskById() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @NotNull String id = task.getId();
        @NotNull final TaskShowByIdResponse taskShowByIdResponse = taskEndpoint.showTaskById(new TaskShowByIdRequest(tokenTest, id));
        Assert.assertNotNull(taskShowByIdResponse);
        Assert.assertEquals(taskName, taskShowByIdResponse.getTask().getName());
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @NotNull final TaskBindToProjectResponse response1 = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(tokenTest, projectId, taskId)
        );
        Assert.assertNotNull(response1);
        task = response1.getTask();
        Assert.assertNotNull(task);
        @NotNull String prjId = task.getProjectId();
        Assert.assertEquals(projectId, prjId);
        @NotNull final TaskUnbindFromProjectResponse response2 = taskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest(tokenTest, taskId)
        );
        task = response2.getTask();
        Assert.assertNotNull(task);
        prjId = task.getProjectId();
        Assert.assertNull(prjId);
    }

    @Test
    public void testUpdateTaskById() {
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(tokenTest, taskName, taskDescription)
        );
        Assert.assertNotNull(response);
        @Nullable TaskDto task = response.getTask();
        @Nullable String id = task.getId();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(tokenTest, id, "newName", "newDescription"));
        Assert.assertNotNull(taskUpdateByIdResponse);
        Assert.assertEquals(id, taskUpdateByIdResponse.getTask().getId());
        Assert.assertEquals("newName", taskUpdateByIdResponse.getTask().getName());
        Assert.assertEquals("newDescription", taskUpdateByIdResponse.getTask().getDescription());
    }

}
