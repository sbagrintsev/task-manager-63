package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull MessageListener listener);

}
