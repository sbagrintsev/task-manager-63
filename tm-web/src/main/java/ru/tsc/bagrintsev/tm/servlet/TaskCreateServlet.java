package ru.tsc.bagrintsev.tm.servlet;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.TaskDTO;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final TaskDTO task = new TaskDTO();
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        @Nullable final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @Nullable final String projectId = req.getParameter("projectId");
        @Nullable final String status = req.getParameter("status");
        @Nullable final String dateStarted = req.getParameter("dateStarted");
        @Nullable final String dateFinished = req.getParameter("dateFinished");

        @Nullable final TaskDTO task = new TaskDTO();
        if (id != null) task.setId(id);
        task.setName(name == null? "" : name);
        task.setDescription(description == null? "" : description);
        task.setProjectId(projectId);
        task.setStatus(Status.valueOf(status));
        task.setDateStarted(DateUtil.toDate(dateStarted));
        task.setDateFinished(DateUtil.toDate(dateFinished));

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
