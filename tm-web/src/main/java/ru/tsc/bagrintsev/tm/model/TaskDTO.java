package ru.tsc.bagrintsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public final class TaskDTO {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String projectId;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStarted;

    @Nullable
    private Date dateFinished;

    @Override
    public String toString() {
        return name + " : " + description +
                "\n\tid: " + getId() +
                "\n\tstatus: " + status +
                "\tstarted: " + DateUtil.toString(dateStarted) +
                "\tfinished: " + DateUtil.toString(dateFinished);
    }

}
