package ru.tsc.bagrintsev.tm.servlet;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        @Nullable final ProjectDTO project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        @Nullable final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @Nullable final String status = req.getParameter("status");
        @Nullable final String dateStarted = req.getParameter("dateStarted");
        @Nullable final String dateFinished = req.getParameter("dateFinished");

        @NotNull final ProjectDTO project = new ProjectDTO();
        if (id != null) project.setId(id);
        project.setName(name == null? "" : name);
        project.setDescription(description == null? "" : description);
        project.setStatus(Status.valueOf(status));
        project.setDateStarted(DateUtil.toDate(dateStarted));
        project.setDateFinished(DateUtil.toDate(dateFinished));

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
