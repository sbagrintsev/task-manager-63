package ru.tsc.bagrintsev.tm.servlet;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        ProjectRepository.getInstance().removeById(id);
        resp.sendRedirect("/projects");
    }

}
