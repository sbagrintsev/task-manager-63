package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.model.TaskDTO;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {return INSTANCE;}

    private TaskRepository() {}

    private long count = 1;

    private final Map<String, TaskDTO> tasks = new LinkedHashMap<>();

    {
        add("Demo");
        add("Test");
        add("Mega");
        add("Beta");
    }

    public void add(String name) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        tasks.put(task.getId(), task);
        count++;
    }

    public void save(TaskDTO task) {
        tasks.put(task.getId(), task);
    }

    public Collection<TaskDTO> findAll() {
        return tasks.values();
    }

    public TaskDTO findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }
    
}
