package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.model.ProjectDTO;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {return INSTANCE;}

    private ProjectRepository() {}

    private long count = 1;

    private final Map<String, ProjectDTO> projects = new LinkedHashMap<>();

    {
        add("Demo");
        add("Test");
        add("Mega");
        add("Beta");
    }

    public void add(String name) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        projects.put(project.getId(), project);
        count++;
    }

    public void save(ProjectDTO project) {
        projects.put(project.getId(), project);
    }

    public Collection<ProjectDTO> findAll() {
        return projects.values();
    }

    public ProjectDTO findById(String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

}
