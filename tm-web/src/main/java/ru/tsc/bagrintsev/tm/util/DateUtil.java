package ru.tsc.bagrintsev.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    @Nullable
    @SneakyThrows
    static Date toDate(final String value) {
        return (value == null || value.isEmpty())? null : format.parse(value);
    }

    @NotNull
    static String toString(final Date date) {
        return date == null? "" : format.format(date);
    }

}
