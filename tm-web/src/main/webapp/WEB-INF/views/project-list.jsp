<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="margin-top: 20px;">PROJECT LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="15%" nowrap="nowrap">ID</th>
        <th width="15%" nowrap="nowrap" align="left">NAME</th>
        <th width="30%" align="left">DESCRIPTION</th>
        <th width="10%" nowrap="nowrap" align="center">STATUS</th>
        <th width="10%" nowrap="nowrap" align="center">START</th>
        <th width="10%" nowrap="nowrap" align="center">FINISH</th>
        <th width="10%" nowrap="nowrap" align="center">EDIT</th>
        <th width="10%" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}" >
        <tr>
            <td>
                <c:out value="${project.id}" />
            </td>
            <td>
                <c:out value="${project.name}" />
            </td>
            <td>
                <c:out value="${project.description}" />
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${project.status.displayName}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStarted}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateFinished}" />
            </td>
            <td align="center">
                <a href="${pageContext.request.contextPath}/projects/edit/?id=${project.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="${pageContext.request.contextPath}/projects/delete/?id=${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>

</table>

<form action="${pageContext.request.contextPath}/projects/create" style="margin-top: 20px; margin-left: 50px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>