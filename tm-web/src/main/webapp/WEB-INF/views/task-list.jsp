<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="margin-top: 20px;">TASK LIST</h1>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="15%" nowrap="nowrap">ID</th>
        <th width="15%" nowrap="nowrap" align="left">NAME</th>
        <th width="30%" align="left">DESCRIPTION</th>
        <th width="10%" nowrap="nowrap" align="center">PROJECT</th>
        <th width="10%" nowrap="nowrap" align="center">STATUS</th>
        <th width="10%" nowrap="nowrap" align="center">START</th>
        <th width="10%" nowrap="nowrap" align="center">FINISH</th>
        <th width="10%" nowrap="nowrap" align="center">EDIT</th>
        <th width="10%" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="task" items="${tasks}" >
        <tr>
            <td>
                <c:out value="${task.id}" />
            </td>
            <td>
                <c:out value="${task.name}" />
            </td>
            <td>
                <c:out value="${task.description}" />
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${task.projectId}" />
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${task.status.displayName}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateStarted}" />
            </td>
            <td align="center">
                <fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateFinished}" />
            </td>
            <td align="center">
                <a href="${pageContext.request.contextPath}/tasks/edit/?id=${task.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="${pageContext.request.contextPath}/tasks/delete/?id=${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>

</table>

<form action="${pageContext.request.contextPath}/tasks/create" style="margin-top: 20px; margin-left: 50px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>