<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>PROJECT EDIT</h1>

<form action="${pageContext.request.contextPath}/projects/edit?id=${project.id}" method="POST">

    <input type="hidden" name="id" value="${project.id}" />

    <p>
        <label>NAME:
            <input type="text" name="name" value="${project.name}" />
        </label>
    </p>

    <p>
        <label>DESCRIPTION:
            <input type="text" name="description" value="${project.description}" />
        </label>
    </p>

    <p>
        <label>STATUS:
            <select name="status">
                <c:forEach var="status" items="${statuses}">
                    <option <c:if test="${project.status == status}">selected="selected"</c:if> value="${status}">
                            ${status.displayName}
                    </option>
                </c:forEach>
            </select>
        </label>
    </p>

    <p>
        <label>DATE START:
            <input type="date" name="dateStarted"
                   value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStarted}" />"/>
        </label>
    </p>

    <p>
        <label>DATE FINISH:
            <input type="date" name="dateFinished"
                   value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateFinished}" />"/>
        </label>
    </p>

    <button type="submit">SAVE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>