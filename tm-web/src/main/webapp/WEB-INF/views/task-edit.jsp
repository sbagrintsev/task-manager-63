<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK EDIT</h1>

<form action="${pageContext.request.contextPath}/tasks/edit?id=${task.id}" method="POST">

    <input type="hidden" name="id" value="${task.id}" />

    <p>
        <label>NAME:
            <input type="text" name="name" value="${task.name}" />
        </label>
    </p>

    <p>
        <label>DESCRIPTION:
            <input type="text" name="description" value="${task.description}" />
        </label>
    </p>

    <p>
        <label>STATUS:
            <select name="status">
                <c:forEach var="status" items="${statuses}">
                    <option
                            <c:if test="${task.status == status}">selected="selected"</c:if> value="${status}">
                            ${status.displayName}
                    </option>
                </c:forEach>
            </select>
        </label>
    </p>

    <p>
        <label>DATE START:
            <input type="date" name="dateStarted"
                   value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateStarted}" />"/>
        </label>
    </p>

    <p>
        <label>DATE FINISH:
            <input type="date" name="dateFinished"
                   value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateFinished}" />"/>
        </label>
    </p>

    <p>
        <label>PROJECT:
            <select name="projectId">
                <option value="">-- // --</option>
                <c:forEach var="project" items="${projects}">
                    <option
                            <c:if test="${project.id == task.projectId}">selected="selected"</c:if> value="${project.id}">
                            ${project.name}
                    </option>
                </c:forEach>
            </select>
        </label>
    </p>

    <button type="submit">SAVE</button>
</form>

<jsp:include page="../include/_footer.jsp"/>