<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Task Manager</title>
    </head>
    <style>

        h1 {
            font-size: 1.6em;
        }

        a {
            color: teal;
        }

        select {
            width: 200px;
        }

        input[type="text"] {
            width: 200px;
        }

        input[type="date"] {
            width: 200px;
        }

    </style>

    <body>

        <table width="100%" height="100%" border="1" style="border-collapse: collapse;">
            <tr>
                <td height="35" width="20%" nowrap="nowrap" align="center">
                    <a href="${pageContext.request.contextPath}/"><b>TASK MANAGER</b></a>
                </td>
                <td width="80%" align="right" nowrap="nowrap" style="padding-right: 30px;">
                    <a href="${pageContext.request.contextPath}/projects">PROJECTS</a>
                    <b>|</b>
                    <a href="${pageContext.request.contextPath}/tasks">TASKS</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="100%" valign="top" style="">
