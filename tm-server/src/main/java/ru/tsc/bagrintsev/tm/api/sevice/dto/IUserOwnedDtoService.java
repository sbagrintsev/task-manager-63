package ru.tsc.bagrintsev.tm.api.sevice.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.AbstractWBSDtoModel;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.SortWBS;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractWBSDtoModel> extends IAbstractDtoService<M> {

    M add(
            @Nullable String userId,
            M record
    ) throws ModelNotFoundException, IdIsEmptyException;

    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException, ModelNotFoundException;

    void clear(@Nullable String userId) throws IdIsEmptyException;

    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException;

    M create(
            @Nullable String userId,
            @Nullable String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException;

    boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException;

    @NotNull List<M> findAll(@Nullable String userId) throws IdIsEmptyException;

    @NotNull
    List<M> findAll(
            @Nullable final String userId,
            @Nullable final SortWBS sort
    ) throws IdIsEmptyException;

    M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    long totalCount(@Nullable String userId) throws IdIsEmptyException;

    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException;

}
