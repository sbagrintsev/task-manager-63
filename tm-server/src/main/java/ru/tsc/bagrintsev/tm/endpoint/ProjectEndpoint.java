package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectTaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDto;
import ru.tsc.bagrintsev.tm.dto.request.project.*;
import ru.tsc.bagrintsev.tm.dto.response.project.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.enumerated.SortWBS;

import java.util.List;

@Controller
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final IProjectTaskDtoService projectTaskService;

    public ProjectEndpoint(
            @NotNull final IUserDtoService userService,
            @NotNull final IAuthDtoService authService,
            @NotNull final IProjectDtoService projectService,
            @NotNull final IProjectTaskDtoService projectTaskService) {
        super(userService, authService);
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectDto project = projectService.changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final ProjectDto project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String sortValue = request.getSortValue();
        @Nullable final SortWBS sort = SortWBS.toSort(sortValue);
        @NotNull final List<ProjectDto> projects = projectService.findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @NotNull final ProjectDto project = projectService.findOneById(userId, id);
        projectTaskService.removeProjectById(userId, project.getId());
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @NotNull final ProjectDto project = projectService.findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final ProjectDto project = projectService.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

}
