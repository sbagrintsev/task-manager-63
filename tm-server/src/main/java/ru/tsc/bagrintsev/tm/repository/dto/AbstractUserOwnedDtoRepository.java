package ru.tsc.bagrintsev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.dto.model.AbstractWBSDtoModel;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDtoRepository<M extends AbstractWBSDtoModel> extends AbstractDtoRepository<M> {

}
