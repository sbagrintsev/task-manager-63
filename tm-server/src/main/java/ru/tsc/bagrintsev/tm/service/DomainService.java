package ru.tsc.bagrintsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.IDomainService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskDtoService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserDtoService;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JACKSON_XML = "./data.jackson.xml";

    @NotNull
    public static final String FILE_JACKSON_JSON = "./data.jackson.json";

    @NotNull
    public static final String FILE_JACKSON_YAML = "./data.jackson.yaml";

    @NotNull
    public static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    private final ITaskDtoService taskService;

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final IUserDtoService userService;

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) throws DomainNotFoundException {
        if (domain == null) throw new DomainNotFoundException();
        taskService.clearAll();
        projectService.clearAll();
        userService.clearAll();
        userService.set(domain.getUsers());
        projectService.set(domain.getProjects());
        taskService.set(domain.getTasks());
    }

    @Override
    public void loadBackup() throws IOException, DomainNotFoundException, ClassNotFoundException {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        final byte @NotNull [] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais)) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    public void loadBase64() throws IOException, ClassNotFoundException, DomainNotFoundException {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte @NotNull [] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais)) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    public void loadBinary() throws IOException, ClassNotFoundException, DomainNotFoundException {
        try (@NotNull final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_BINARY))) {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @Override
    public void loadJacksonJSON() throws IOException, DomainNotFoundException {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    public void loadJacksonXML() throws IOException, DomainNotFoundException {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public void loadJacksonYAML() throws IOException, DomainNotFoundException {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_YAML)));
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Override
    public void loadJaxbJSON() throws JAXBException, DomainNotFoundException {
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty("eclipselink.media-type", "application/json");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public void loadJaxbXML() throws JAXBException, DomainNotFoundException {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public void saveBackup() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false)) {
            oos.writeObject(domain);
            final byte @NotNull [] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @Override
    public void saveBase64() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false)) {
            oos.writeObject(domain);
            final byte @NotNull [] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @Override
    public void saveBinary() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        try (@NotNull final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false))) {
            oos.writeObject(domain);
        }
    }

    @Override
    public void saveJacksonJSON() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_JSON)) {
            fos.write(json.getBytes());
            fos.flush();
        }
    }

    @Override
    public void saveJacksonXML() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_XML)) {
            fos.write(xml.getBytes());
            fos.flush();
        }
    }

    @Override
    public void saveJacksonYAML() throws IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_YAML)) {
            fos.write(yaml.getBytes());
            fos.flush();
        }
    }

    @Override
    public void saveJaxbJSON() throws JAXBException {
        System.setProperty("jakarta.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("eclipselink.media-type", "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

    @Override
    public void saveJaxbXML() throws JAXBException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

}
