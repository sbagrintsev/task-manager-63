package ru.tsc.bagrintsev.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserSetRoleRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private Role role;

    public UserSetRoleRequest(@Nullable String token) {
        super(token);
    }

    public UserSetRoleRequest(@Nullable String token, @Nullable String login, @Nullable Role role) {
        super(token);
        this.login = login;
        this.role = role;
    }

}
