package ru.tsc.bagrintsev.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String oldPassword;

    @Nullable
    private String newPassword;

    public UserChangePasswordRequest(@Nullable String token) {
        super(token);
    }

    public UserChangePasswordRequest(@Nullable String token, @Nullable String oldPassword, @Nullable String newPassword) {
        super(token);
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

}
