package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractResultResponse {

    @Nullable
    private UserDto user;

    public UserViewProfileResponse(@Nullable final UserDto user) {
        this.user = user;
    }

    public UserViewProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
